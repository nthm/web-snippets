import dotenv from 'dotenv';
import debug from 'debug';
import path from 'path';

import { typedKeys, typedEntries } from './helpers/typedKeysEntries';
import { printList as pL } from './helpers/printList';

type TConfigAll = {
  /** Sets up an HTTP redirection server via HTTP 301 */
  REDIRECT_TO_HTTPS: boolean;
  HTTP_PORT: number;
  HTTPS_PORT: number;

  /** Network interface such as 0.0.0.0 or localhost */
  LISTEN_ADDRESS: string;

  /** Certificate root for SNI. Relative paths use __dirname at setup */
  CERTIFICATE_PATH: string;

  /** For SQL. Likely use `DB_USER:DB_PASS@DB_SERVER` */
  DB_CONNECTION_URL: string;

  /** Express session key */
  SESSION_KEY: string;

  /** If the resolved config (given to the application) should be printed */
  PRINT_CONFIG: boolean;
};

// This is also a compile time assert to check the strings of configKeys
type TConfig = Pick<TConfigAll, typeof configKeys[number]>;

const configKeys = [
  'REDIRECT_TO_HTTPS',
  'HTTP_PORT',
  'HTTPS_PORT',
  'LISTEN_ADDRESS',
  'CERTIFICATE_PATH',
  'DB_CONNECTION_URL',
  'SESSION_KEY',
] as const;

// Defaults
const optional: Partial<TConfigAll> = {
  REDIRECT_TO_HTTPS: true,
  HTTP_PORT: 5080,
  HTTPS_PORT: 5443,
  CERTIFICATE_PATH: './',
  LISTEN_ADDRESS: '0.0.0.0',
  PRINT_CONFIG: true,
};

const log = debug('config');
let initialized = false;
const _config: Partial<TConfig> = {};
const config = new Proxy<Readonly<TConfig>>(_config as TConfig, {
  set(target, key: unknown, value: unknown) {
    throw new Error('Config is readonly');
  },
  // Using `key: keyof TConfig` is kind of a lie. Not sure how to use `unknown`
  get(target, key) {
    if (!initialized) {
      throw new Error(
        `Config was asked for '${String(key)}' before setupConfig()`);
    }
    if (!isConfigKey(key, configKeys)) {
      // This is OK to throw because it means there's a bug. Config reads should
      // always be static and safe
      throw new Error(`Config key '${String(key)}' doesn't exist`);
    }
    const value: unknown = target[key];
    if (isConfigKey(key, ['CERTIFICATE_PATH', 'SESSION_KEY'])) {
      if (value) {
        log(`Removing sensitive key '${key}' after initial read`);
        delete target[key];
      } else {
        log(`Reporting read config key '${key}'`);
        console.trace(`Sensitive key read: ${key}`);
      }
    }
    return value;
  },
});

function setupConfig() {
  const envs = dotenv.config();
  // BUG: Debug doesn't work with dotenv under `node -r esm`
  // Use an empty `DEBUG=` to disable everything. Without it, defaults to '*'
  debug.enable((envs.parsed && envs.parsed.DEBUG) || '*');
  log(`Loaded into process.env: ${pL(Object.keys(envs.parsed || {}))}`);

  // Typescript isn't great with Object.assign()
  // tslint:disable-next-line: no-object-literal-type-assertion
  const all = { ...optional, ...(envs.parsed as {} || {}) } as TConfigAll;

  // Overwrites and runtime validation
  const unknown: string[] = [];
  const seen: (keyof TConfigAll)[] = [];
  let printConfig = optional.PRINT_CONFIG;

  // Validations
  for (const [key, value] of Object.entries(all)) {
    // Enforce config instead of process.env which leaks to node_modules
    delete process.env[key];

    if (!isConfigKey(key, [...configKeys, 'PRINT_CONFIG'])) {
      unknown.push(key);
      continue;
    }
    seen.push(key);

    if (key === 'REDIRECT_TO_HTTPS') {
      _config[key] = toBoolean(value);
      continue;
    }
    if (key === 'PRINT_CONFIG') {
      printConfig = toBoolean(value);
      continue;
    }
    if (key === 'HTTPS_PORT' || key === 'HTTP_PORT') {
      _config[key] = Number(value);
      continue;
    }
    if (key === 'CERTIFICATE_PATH') {
      _config[key] = path.resolve(__dirname, String(value));
      continue;
    }

    // This will helpfully complain if you're missing type validations above
    _config[key] = String(value);
  }

  const needed = configKeys.filter(key =>
    !typedKeys(optional).includes(key) && !seen.includes(key));

  if (unknown.length || needed.length) {
    throw new Error([
      'Configuration validation issues:',
      unknown.length
        ? `- Unknown config keys ${pL(unknown)}`
        : '',
      needed.length
        ? `- Must provide ${pL(needed)} via environment`
        : '',
    ].filter(Boolean).join('\n'));
  }

  if (printConfig) {
    log('Resolved config:');
    log(typedEntries(_config).map(([key, value]) =>
      ` - ${key}: ${isConfigKey(key, ['SESSION_KEY'])
        ? '[REDACTED]'
        : value
      }`).join('\n'));
  }

  initialized = true;
}

function toBoolean(value?: string | number | boolean) {
  return (
    /y(?:es)?|true/i.test(String(value)) ? true :
      /no?|false/i.test(String(value)) ? false :
        Boolean(value)
  );
}

function isConfigKey<T extends (keyof TConfigAll)>(
  key: unknown,
  configSet: Readonly<T[]>
): key is T {
  return configSet.includes(key as T);
}

export { config, setupConfig };
