import https from 'https';
import http from 'http';
import ip from 'internal-ip';
import debug from 'debug';

import { config, setupConfig } from './config';
// Setup and parse configuration before modules will use it
setupConfig();

// All other imports that use config...
import { x, y, z } from './use/x';
import { a, b } from './use/a';

const log = debug('server');

const app: http.RequestListener = (req, res) => {
  // ...
}

(async () => {
  const host = config.LISTEN_ADDRESS === 'localhost'
    ? config.LISTEN_ADDRESS
    : await ip.v4();

  const httpServer = http.createServer(
    config.REDIRECT_TO_HTTPS
      ? (req, res) => {
        res.writeHead(301, {
          Location: `https://${req.headers.host}${req.url}`,
        });
        res.end();
      }
      : app);
  httpServer.listen({
    host: config.LISTEN_ADDRESS,
    port: config.HTTP_PORT,
  }, () => {
    const serverType = config.REDIRECT_TO_HTTPS
      ? 'HTTPS redirection server'
      : 'Server';
    log(`${serverType} listening on http://${host}:${config.HTTP_PORT}`);
  });

  const httpsServer = https.createServer({
    SNICallback(serverName, callback) {
      // ...
    },
  }, app);
  httpsServer.listen({
    host: config.LISTEN_ADDRESS,
    port: config.HTTPS_PORT,
  }, () => {
    log(`Server listening on https://${host}:${config.HTTPS_PORT}`);
  });
})();
