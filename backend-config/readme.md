# Config

Config cookbook/example for an application's startup. Uses dotenv. Lets you set
optional/default values; throws an error about unknown or missing config
settings; tries to be a bit more secure by using a `Proxy()` to be readonly and
track reads; avoids `process.env` to not leak settings to `node_modules`. Prints
the resolved config during setup, with redactions.

Focuses heavily on TypeScript being able to infer types everywhere and narrow
the types as you walk through the code. Hovering on variables like `key` in your
editor should show possible states, and this is really helpful for catching
issues at compile time - it shouldn't be possible to have typos in the keys 🤞

---

I once got a bit carried away with this. See MR !1 and its versions 1-12. Wanted
to support nearly any use case (which can't be done, really) such as needing A
or B; dependencies between config values; selectors that take some values, use
them, and then delete them... It never ends.

Needed to step back. This is a _cookbook_ not a library. Reusable copy/paste
block with minimal edits is better. Everything is possible, and you can add the
complexity you need for your use case.

_TODO:_ An edit I'd recommend but haven't implemented is validation _errors_.
These could be range errors, or if a path isn't found, etc. Hard to not go down
the rabbit hole though 😏
