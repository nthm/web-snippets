import http from 'http';
import debug from 'debug';

import express from 'express';
import session from 'express-session';

import { setupWebsocketHandler } from './websockets';

const sessionParser = session({
  name: 'sid',
  resave: false,
  saveUninitialized: true,
  secret: process.env.SESSION, // TODO:
  store: new session.MemoryStore(),
});


const log = debug('server');
const app = express();
app.use(sessionParser);
app.use((req, res, next) => {
  if (req.session && !req.session.created) {
    req.session.created = (new Date()).toLocaleString;
  }
  return next();
});

// TODO: Define routes here
app.use(...);

(async () => {
  // See backend-config (in this repo) or Branchpoint for better examples of
  // this setup including HTTPS redirection

  const server = http.createServer(app);
  server.listen(3000, () => {
    log(`Server listening on https://localhost:3000`);
  });
  setupWebsocketHandler(server, sessionParser);
})();
