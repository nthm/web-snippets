# Websockets

These tie into the Express session of your web server to collect metrics in the
same way you would with real HTTP requests.

This doesn't go over the database or storing metrics. They're just displayed to
the console for now.
