import WebSocket from 'ws';

// These are all for types
import { RequestHandler, Request, Response } from 'express';
import { Socket as NetSocket } from 'net';
import https from 'https';
import http from 'http';
import debug from 'debug';

// I won't do cross-snippet-folder linking, but this is unionExtraction.ts from
// the `typescript-snips` folder of this repository
import { MetricEvent } from './metricsTypes';

const log = debug('wss');

// TODO: You should be using a config ;)
const HEARTBEAT_MS = 5000;

const wss = new WebSocket.Server({
  // Don't use `WebSocket.Server({ server })` since we already have a server to bind with
  noServer: true,
  // Provide wss.clients as a Set() of WebSocket instances
  clientTracking: true,
});

// Detect closed and broken connections (lifted from repo FAQ)
// TODO: Likely store the session objects weakly in here
const socketInfoMap = new WeakMap<WebSocket, { isAlive: boolean }>();

wss.on('connection', function wsConnection(
  ws: WebSocket, upgradeRequest: Response, session: Express.Session
) {
  // Mark as alive when created
  socketInfoMap.set(ws, { isAlive: true });
  log(`Session keys: "${Object.keys(session).join('", "')}"`);
  const sessionID = session.id;

  if (wss.clients.size === 1) {
    log('First connection, starting broadcast loop');
    startHeartbeatBroadcast();
  }

  ws.on('message', (message: WebSocket.Data) => {
    if (typeof message === 'string') {
      const obj: MetricEvent = JSON.parse(message);
      // This is where the magic happens
      const withSession: object = { sessionID, ...obj };

      if (obj.type === 'ERROR') {
        log(withSession)
        console.error(withSession);
        return;
      }

      log(withSession);
      return;
    }

    // Must be an ArrayBuffer or something...
    const safeMessage: string = JSON.stringify(message);
    log(safeMessage);
  });

  ws.on('pong', () => {
    socketInfoMap.set(ws, { isAlive: true });
  });

  ws.on('close', () => {
    log(`Disconnected: ${sessionID}`);
    log(`Websocket count: ${wss.clients.size}`);
    // There's a chance this runs _before_ the internal ws.on('close') in wss
    if (wss.clients.size === (wss.clients.has(ws) ? 1 : 0)) {
      log('Last client disconnected, ending broadcast loop');
      clearTimeout(broadcastInterval);
    }
  });

  // Sign of life
  const ip = upgradeRequest.connection.remoteAddress;
  log(`New websocket: ${sessionID} at IP ${ip}`);
  log(`Websocket count: ${wss.clients.size}`);
});

// Send 💓 every 5s to all clients
const sendPing = (ws: WebSocket) => ws.ping('\uD83D\uDC93\n\n');
let broadcastInterval: NodeJS.Timeout;

function startHeartbeatBroadcast() {
  broadcastInterval = setInterval(() => {
    wss.clients.forEach(ws => {
      const info = socketInfoMap.get(ws);
      if (!info || info.isAlive === false) {
        return ws.terminate();
      }
      // Assume not responsive until pong
      socketInfoMap.set(ws, { isAlive: false });
      if (ws.readyState === WebSocket.OPEN) {
        // Ping/Pong might not be appropriate; it ignores other signs of life
        // Also, you can't see pings in browsers since they're not considered
        // real messages; they're lower level than that
        sendPing(ws);
      }
    });
  }, HEARTBEAT_MS);
}

// Don't use deprecated `verifyClient`
// https://github.com/websockets/ws/issues/377#issuecomment-462152231
type Server = http.Server | https.Server;
function setupWebsocketHandler(server: Server, sessionParser: RequestHandler) {
  log('Registered `upgrade` event for webserver');
  server.on('upgrade', async function wsUpgrade(
    request: Request, socket: NetSocket, head: Buffer
  ) {
    log('Received upgrade request');
    // Extract the session outside of Express middleware...
    // https://github.com/expressjs/session/issues/592 ❌
    // https://github.com/HenningM/express-ws/blob/master/src/index.js ❌

    sessionParser(request, {} as Response, () => {
      if (!request.session) {
        // No session
        // TODO: This should be considered unusual and logged
        log('Request has no session');
        socket.destroy();
        return;
      }
      log(`Parsed session: ${request.sessionID}`);
      const session: Express.Session = request.session;
      wss.handleUpgrade(request, socket, head, function wsDoneUpgrade(ws) {
        // TODO: Session might keep the closure alive. Bad for GC. Use WeakMap()?
        wss.emit('connection', ws, request, session);
      });
    });
  });
}

export { setupWebsocketHandler };
