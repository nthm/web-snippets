FROM pstauffer/curl AS downloader

ENV URL=http://downloads.server.xyz/files
ENV FLAGS='-OL -H "Private-Token: XXX"'
WORKDIR /opt/app/data/
RUN echo "Downloading supporting JSON and database files" \
    && curl $FLAGS $URL/provinces.json \
    && curl $FLAGS $URL/currencies.json \
    && curl $FLAGS $URL/db.sqlite

FROM node:12-alpine

WORKDIR /opt/app/
COPY dist ./dist/
COPY --from=downloader /opt/app/data/ ./data/
# Needed for `npm run start-compiled`. Safer to have the lock file too
COPY package*.json ./
# Copy (instead of installing dependencies, which would need npm and more)
COPY node_modules ./node_modules/
# Depending on the original host where `npm install` was run, this could be outdated
RUN ./node_modules/node-pre-gyp/bin/node-pre-gyp install sqlite3 --directory=./node_modules/sqlite3

EXPOSE 3000
# See Branchpoint as an example TS app that compiled down to JS as a DIST out
ENTRYPOINT ["npm", "run", "start-compiled"]
