#! /usr/bin/env sh
set -e

export ENVIRONMENT=${ENVIRONMENT:-'production'}
export LISTEN_PORT=${LISTEN_PORT:-80}
export API_URL=${API_URL:-'http://api-server:5000'}

env
echo ""
echo "Resolved config:"

cat > /etc/nginx/nginx.conf <<EOF
worker_processes auto;
error_log /var/log/nginx/error.log;

events {
    worker_connections 1024;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    server {
        listen ${LISTEN_PORT} default_server;
        root /opt/app/;

        location / {
            try_files \$uri /index.html;
        }

        location /assets/config/ {
            add_header EnvironmentName ${ENVIRONMENT};
        }

        location /api {
            proxy_pass ${API_URL};
        }
    }
}
EOF

cat /etc/nginx/nginx.conf
exec $(which nginx) -g "daemon off;"
