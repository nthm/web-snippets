See commit history for last update

Used http://latitude.to/ for looking up lat/lngs

## Google
https://cloud.google.com/about/locations/#regions-tab
https://cloud.google.com/compute/docs/regions-zones/#available

## Azure
https://azure.microsoft.com/en-us/global-infrastructure/regions/
https://azure.microsoft.com/en-us/global-infrastructure/geographies/

Skipped the government and department of defense locations

## Amazon
https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.RegionsAndAvailabilityZones.html