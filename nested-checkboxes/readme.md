# nests and checkboxes

![Demo showing many checkbox trees in different
orders](./screenshot.png "Nests are of cloud provider regions")

Developed for a publicly accessible Voronoi map of cloud provider (AWS, Google,
Azure) regions. This doesn't only nest checkboxes, rather, it takes a flat array
of JSON documents, and given an order of its fields, it produces a `Map()` tree
to store them.

![a console.log of the nest](./nest.png "You don't need to end the
tree with a Set(), I just happened to not attach objects in this one.")

HTML is built by traversing the tree recursively, and each checkbox is given a
data attribute set to the absolute path back in the data structure.

Nesting doesn't come naturally to checkboxes. All implementations I found were
either designed to work only with one nesting level, or they relied on elements
following a strict hierarchy to make use of the `.parentNode` and `.children`
attributes.

TODO: Explain the event handling and lack of strict HTML structure

A version of the site was last hosted here:
https://tutelatechnologies.github.io/voronoi-maps/nested-location-selection/
