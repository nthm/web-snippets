import { h, Component, render } from 'preact'
import PropTypes from 'prop-types'
import './style.css'

// this is a lot like the <Router /> component. store a reference to the one
// existing notification container to later call `setState()` on
let initializedNoteContainer = null

const notify = text => {
  if (!initializedNoteContainer) {
    console.error('no notification container')
    return
  }
  // low-key document.appendChild
  render(<Note text={text} />, initializedNoteContainer.element)
}

// notes are self destructing
class Note extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    timeout: PropTypes.number,
  }
  timeout = this.props.timeout || 5000 // ms
  opacityTimeout = 500 // ms

  componentDidMount() {
    this.element.style.opacity = 1
    setTimeout(() => {
      this.element.style.opacity = 0
      // remove the data and element after opacity transition
      setTimeout(() => {
        render('', initializedNoteContainer.element, this.element)
      }, this.opacityTimeout)
    }, this.timeout)
  }
  render() {
    return (
      <div
        ref={element => (this.element = element)}
        className='note'
      >
        {this.props.text}
      </div>
    )
  }
}

class NoteContainer extends Component {
  constructor() {
    super()
    initializedNoteContainer = this
  }
  render() {
    return (
      <div
        ref={element => (this.element = element)}
        id='note-container'
      />
    )
  }
}

export { NoteContainer, notify }
