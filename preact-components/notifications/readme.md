# notifications

There are a few approaches to this. Some people have opted to use an event-based
structure (similar to Redux) where notifications are queued by emitting events,
and then subscribers are told to show the content.

This was seen in _react-toastify_. Here's some links, including the source of
their event store (which is a Map of subscriptions to callbacks):

https://fkhadra.github.io/react-toastify/
https://github.com/fkhadra/react-toastify/blob/master/src/toaster.js
https://github.com/fkhadra/react-toastify/blob/master/src/utils/EventManager.js
https://github.com/fkhadra/react-toastify/blob/master/src/components/ToastContainer.js

Another library I used for inspiration was _react-notify-toast_:

https://github.com/jesusoterogomez/react-notify-toast

Which calls `ReactDOM.render` (!) to mount/render a notification at a given DOM
node. In their case, an already mounted notification container.

This raised some red flags for me, so I read more into portals:

https://github.com/facebook/react/issues/10294
https://reactjs.org/docs/portals.html

Which are super neat and make a lot of sense. Official documentation is great,
and mentions the use case for modals, dialogs, hovercards, tooltips, and
notifications. However, the library `react-notify-toast` is _not_ using them,
and still renders notifications by finding a DOM node (using
`document.querySelector()`) and calling `ReactDOM.render(<Toast .../>, node)`.
Many people are using it in production...

Looking into portals for Preact, there's a small 1kb extension by the official
Preact developer which is well documented with GIFs explaining nearly the exact
use case I was going for. Surprisingly, it uses `render` just like the
notification library does with `ReactDOM.render`, so I've skipped portals and
just use that as well.
