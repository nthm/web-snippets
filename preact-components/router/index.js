import { Component } from 'preact'
import PropTypes from 'prop-types'

let initializedRouter = null

const route = newUrl => {
  if (!initializedRouter) {
    console.error('router not initialized')
    return
  }
  console.log('routing to', newUrl)
  window.history.pushState(null, null, newUrl)
  initializedRouter.setState({ url: newUrl })
}

class Router extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    routes: PropTypes.object.isRequired,
  }
  state = {
    url: window.location.pathname || '/',
  }
  constructor(props) {
    super(props)
    initializedRouter = this

    // stop the page from refreshing. note: could store data in event.state
    window.addEventListener('popstate', () => {
      // don't call this.redirect because it'll pushState as you try to pop it
      this.setState({ url: window.location.pathname })
    })

    // from preact-router
    const routes = Object.keys(props.routes)
    window.addEventListener('click', e => {
      // ignore events the browser takes care of already:
      if (e.ctrlKey || e.metaKey || e.altKey || e.shiftKey || e.button !== 0) {
        return
      }
      let node = e.target
      do {
        if (String(node.nodeName).toUpperCase() === 'A') {
          const href = node.getAttribute('href')
          if (!href || !routes.includes(href)) {
            continue
          }
          // link is handled by the router, so prevent defaults and exit
          this.redirect(href)
          if (e.stopImmediatePropagation) e.stopImmediatePropagation()
          if (e.stopPropagation) e.stopPropagation()
          e.preventDefault()
          return
        }
      } while ((node = node.parentNode))
    })
  }
  redirect = route
  render() {
    const { onChange, routes } = this.props
    onChange()

    const path = this.state.url.split('?')[0]
    console.log('path:', path)

    // can't use a switch case because routes aren't defined here
    for (const pathToMatch in routes) {
      if (path === pathToMatch) {
        return routes[path]()
      }
    }
    // default case, 404
    setTimeout(() => this.redirect('/'), 1000)
    console.log('path did not match, redirecting to /')
  }
}

export default Router
export { route }
