# router

Developed because `preact-router` was large, complicated, and did not appear to
allow routes that didn't render content. After some research, I found people who
were successfully using `switch` statements as routers - this is a similar idea
but implemented as a Preact component that accepts an object of routes.

The only file is `router.js` which supplies both a default export of a component
and a `route()` method for imperatively redirecting someplace else. It's
designed to be the only router on the page, only be initialized (mounted) once,
and to use the native `window.history` API.

Example usage as a typical top-level router is in `usage.js` which is a trimmed
down top-level component (think `src/index.js`) from another project.
