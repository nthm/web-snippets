import { h, render } from 'preact'
import Router, { route } from '.'
import { Login, SignUp, ForgotPassword } from './pages'
import './style.css'

// could be moved to a router state
let params = {}

const App = () =>
  <main>
    <h2>Authentication Portal</h2>
    <p>Reused as an example of how to use the router component</p>
    <Router
      onChange={() => {
        // update an object of all the URL query parameters for each page
        params = decodeURLParams()
      }}
      routes={{
        '/': () => <Login />,
        '/signup': () => <SignUp />,
        '/confirmForgotPassword': () => {
          if (!('token' in params)) {
            console.error('/confirmForgotPassword without a token. redirecting')
            route('/')
            return
          }
          return <ForgotPassword
            code={params.code}
          />
        },
        '/confirmEmail': () => {
          if (!('token' in params)) {
            console.error('/confirmEmail without a token. redirecting')
            route('/')
            return
          }
          import('./auth/cognito')
            .then(module => module.confirmEmail(params.code))
            .then(() => {
              route('/')
              showNotification('Thanks for confirming your email')
              return
            })
            .catch(showNotification)
        },
      }}
    />
  </main>

render(<App />, document.body)
