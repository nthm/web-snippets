# Snips

For projects that are too small for their own repo (at least for now).

A small collection of modules developed for other projects, but made general
enough to be reused elsewhere.

Each project is in its own folder and is mostly a cookbook-style of bits of
example code.

## Server startup config (TS)

A bit better than dotenv. Lets you set optional/default values; throws an error
about unknown or missing config settings; tries to be a bit more secure by using
a `Proxy()` to be readonly and track reads; avoids `process.env` to not leak
settings to `node_modules`. Prints the resolved config during setup, with
redactions.

Focuses heavily on TypeScript being able to infer types everywhere and narrow
the types as you walk through the code. Hovering on variables like `key` in your
editor should show possible states, and this is really helpful for catching
issues at compile time - it shouldn't be possible to have typos in the keys 🤞

## Indexed search (JS)

Entirely client side

Developed for [engr-directory](https://gitlab.com/nthm/engr-directory) to search
buildings for rooms, staff, clubs, and other information. Most notes about its
development are in the original repository.

![Example showing a searched query and dev tools](./search/screenshot.png "If
only half my applications looked this clean")

## Nested checkboxes UI (JS)

Developed at Tutela Technologies for converting a `Map()` into a nested tree of
options used for filtering data. Supports expanded and collapsed regions.
Screenshot below is of cloud provider regions.

![Demo showing many checkbox trees in different
orders](./nested-checkboxes/screenshot.png "Nests of cloud provider regions")
