/* globals Directory */

let results = new Set()
let trimmed = ''

let hot = false
let wantsToDraw = false

const files = ['spaces', 'info', 'staff'].map(name => fetch(`data/${name}.json`)
  .then(res => res.json()))

Promise.all(files).then(bundle => Directory.buildIndex(...bundle))

document.addEventListener('DOMContentLoaded', event => {
  const input = document.querySelector('input')
  const main = document.querySelector('div#results')

  function draw() {
    // if display is still cooling down, leave
    if (hot) {
      console.log('...still cooling down')
      wantsToDraw = true
      return
    }
    console.log('drawing (no redraws for 100ms)')
    const fragment = document.createDocumentFragment()
    const p = document.createElement('p')
    p.innerText = `Results: ${results.size}`
    fragment.appendChild(p)

    let count = 0
    for (const entry of results) {
      if (count++ === 10) {
        break
      }
      const box = document.createElement('pre')
      box.innerText = JSON.stringify(entry, null, 2)
      fragment.appendChild(box)
    }
    main.innerHTML = ''
    main.appendChild(fragment)

    hot = true
    wantsToDraw = false
    setTimeout(() => {
      hot = false
      if (wantsToDraw) {
        draw()
      }
    }, 200)
  }

  input.addEventListener('input', event => {
    const value = input.value.trim()
    // TODO: say 'Type to search' and show examples if value === ''
    if (trimmed === value) {
      return
    }
    trimmed = value
    results = Directory.search(value) // is a Set()
    draw()
  })
})
