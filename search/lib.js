// eslint-disable-next-line no-unused-vars
const Directory = Object.freeze({
  // note: could have used a Proxy here as a mixed data structure
  index: new Map(),
  lists: {
    spaces: new Map(),      // room --> { space }
    tags: new Map(),        // tag --> set of [ { space } ... ]
    departments: new Map(), // department code --> set of [ { space } ... ]
    staff: new Set(),       // set of [ { staff } ... ]
  },
  relate(fields, object) {
    // lowercase each term, split on spaces and a few things
    const sanitized = fields.map(x => {
      return x.toLowerCase().replace(/[,.:;!?-_()]/g, ' ').split(/\s+/)
    })
    // flatten array results
    const text = [].concat.apply([], sanitized)
    for (const token of new Set(text)) {
      for (let i = 0, len = token.length; i < len; ++i) {
        // add every prefix substring to the index
        const substring = token.substr(0, i + 1)
        const set = this.index.get(substring) || new Set()
        this.index.set(substring, set.add(object))
      }
    }
  },
  buildIndex(spaces, info, staff) {
    console.time('index')
    // see exampleData.md for the structure of each JSON
    console.log('indexing',
      spaces.length, 'spaces;',
      Object.keys(info).length, 'info objects, and',
      Object.keys(staff).length, 'staff'
    )
    for (const obj of spaces) {
      const room = obj.room
      this.lists.spaces.set(room, obj)

      const fields = [room]
      let dept = obj.dept
      if (dept) {
        fields.push(dept)
        const deptList = this.lists.departments.get(dept) || new Set()
        this.lists.departments.set(dept, deptList.add(obj))
      }
      if ('type' in obj) {
        fields.push(obj.type)
      }
      if (room in info) {
        // don't nest the info object, merge it
        for (const [key, val] of Object.entries(info[room])) {
          // this could be interestingly destructive
          obj[key] = val
          fields.push(val)
        }
        if ('tags' in info[room]) {
          // be less forgiving with spacing of tags
          info[room].tags.split(' ').forEach(tag => {
            const tagList = this.lists.tags.get(tag) || new Set()
            this.lists.tags.set(tag, tagList.add(obj))
          })
        }
        delete info[room]
      }
      this.relate(fields, obj)
      if (room in staff) {
        obj.staff = staff[room]
        delete staff[room]
        for (const person of obj.staff) {
          // reverse link to the space, but not circularly
          person.room = room
          this.lists.staff.add(person)
          this.relate(Object.values(person), person) // added as first result
          this.relate([person.name], obj) // as second result
        }
      }
    }
    console.timeEnd('index')
    ;[staff, info].forEach(obj => {
      const length = Object.keys(obj).length
      if (length > 0) {
        console.warn(`linked object has ${length} unmerged entries`)
        console.log('object:', obj)
      }
    })
    console.log('indexed', this.index.size, 'strings')
  },
  search(query) {
    console.time('search')
    const tokens = query.trim().toLowerCase().split(/\s+/)

    // initial token determines largest result set
    let results = this.index.get(tokens.shift()) || new Set()

    // for all subsequent tokens reduce the set
    for (const token of tokens) {
      const tokenSet = this.index.get(token) || new Set()
      results = new Set([...results].filter(x => tokenSet.has(x)))
    }
    console.timeEnd('search')
    return results
  },
})
