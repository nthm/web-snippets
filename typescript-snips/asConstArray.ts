// const Fields: ValidFields = [...] produces a circular reference error
export const Fields = [
  'Name',
  'Address',
  'City',
  'Country',
  'Province',
  'Postal Code',
  'Currency',
] as const;

export type ValidFields = typeof Fields[number];
