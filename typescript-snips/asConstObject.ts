import { typedKeys } from './typedKeysEntries';

// Here's an example using an object, where `const` restricts the properties to
// be `readonly`

const Colours = {
  RED: {
    FILL: '#FFC7CE',
    FONT: '#9C0006',
  },
  YELLOW: {
    FILL: '#FFEB9C',
    FONT: '#DF8C00',
  },
  GREEN: {
    FILL: '#C6EFCE',
    FONT: '#007B74',
  },
} as const;

// Note that Object.keys() wouldn't work here since it's reduced to "string"
const keys = typedKeys(Colours);

export type ValidColour = typeof keys[number];

export type ObjectWithKeysOfColours = {
  [key in ValidColour]: string;
};
