export { };

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            SESSION: string;
            SQLITE_PATH: string;

            // Etc.
        }
    }
    interface Array<T> {
        includes<U extends (T extends U ? unknown : never)>(searchElement: U, fromIndex?: number): boolean;
        filter<U extends T>(pred: (a: T) => a is U): U[];
    }
}
