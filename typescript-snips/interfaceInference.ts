import { ValidColour } from './asConstObject';

interface ISomeState {
  a: string;
  b: number;
  colourMapping: {
    // Optionally not _all_ colours
    [ker in ValidColour]?: string;
  };
}

interface ISomeOtherState {
  name: string;
  postList: object[];
}

export interface IStateProvider<T> {
  readonly state: Readonly<T>;
  readonly setState: (changes: Partial<T>) => void;
}

export interface IStateConsumer<S> {
  // Not supported in TS: https://www.typescriptlang.org/docs/handbook/interfaces.html
  // new(service: S);
  readonly state: InferStateFromService<S>;
  readonly setState?: (changes: Partial<InferStateFromService<S>>) => void;
}

export type InferStateFromService<T> = T extends IStateProvider<infer R> ? R : never;

// Usage

export class DataService implements IStateProvider<ISomeState> {
  constructor() { }

  // Using `as const` means you can't do `state.a = ...`. You could also do this
  // with `Readonly<ISomeState>`. You'd think this was provided by line 18 in
  // IStateProvider - it's not

  // This is important since, unless I was using Proxy(), there's no opportunity
  // to call subscribers

  // Initial state
  public state = {
    a: 'Initial values',
    b: 10,
    colourMapping: {
      RED: '...',
    },
    // TODO: This is actually kinda broken. Using `as const` breaks away from
    // using ISomeState _without_ raising an error about the `implements`...
  } as const;

  public setState(data: Partial<ISomeState>) {
    if (data.a) {
      // Clean up value specifically. Overwrite/reassign it or just validate the
      // incoming data; i.e negate or ignore negative numbers...

      data.a = data.a.toLocaleUpperCase();
    }

    if (data.b && data.b < 0) {
      data.b = 0;
    }

    // Merge the partial state update onto previous state
    Object.assign(this.state, data);

    // Call all subscribers (however you do that in your app)
    // for (const sub of sub) { }
  }
}

// This will complain if `state` or `setState` are missing
export class Component implements /* OnInit, */ IStateConsumer<DataService> {
  constructor(
    private stateService: DataService
  ) { }

  // TODO: This is really sad. The _entire_ point of using IStateConsumer is so
  // these don't need type definitions

  // Types are not inferred contextually via `implements` due to issues with
  // backwards compatibility. This means type checking only happens if not
  // implicitly any 😢 https://github.com/microsoft/TypeScript/pull/6118

  public state!: InferStateFromService<DataService>;
  public setState!: (changes: Partial<InferStateFromService<DataService>>) => void;

  // ...
}
