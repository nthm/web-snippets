import { ValidFields } from './asConstArray';

type FieldKeys =
  | 'NAME'
  | 'ADDRESS'
  | 'CITY'
  | 'ZIP CODE'
  | 'COUNTRY';

// This is valid
const someVar: FieldKeys = 'ADDRESS';

export type MetricEvent =
  | {
    type: 'START'
    initialURL: string
    applicationVersion: string
  }
  | {
    type: 'ELEMENT_CLICK'
    elementXPath: string
  }
  | {
    type: 'NAVIGATION_EVENT'
    toURL: string
  }
  // Specific app-feature metric logging
  | {
    type: 'AUTOMAPPED_COLUMNS'
    columnMapping: {
      original: string,
      matched: ValidFields
    }[]
  }
  | {
    type: 'ERROR'
    // Properties of Error
    name: string;
    message: string;
    stack?: string;
  };

// This is valid too, making: "START" | "ELEMENT_CLICK" | "NAVIGATION_...
const someTypeVar: MetricEvent['type'] = 'ERROR';

// From the above object union cut out type from each object
type ExcludeTypeField<A> = { [K in Exclude<keyof A, 'type'>]?: A[K] };

const sendMetricEvent = <T extends MetricEvent['type']>(
  type: T,
  payload: ExcludeTypeField<Extract<MetricEvent, { type: T }>>
): void => {
  // Send to the server
};

// Now this autocompletes the second param depending on the first param:
sendMetricEvent('NAVIGATION_EVENT', {
  toURL: '...',
});
